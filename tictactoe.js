let menuDemarrage = document.getElementById("menu-demarrage");
let gameIn = document.getElementById("game-in");
let playerX = document.getElementById("player-x");
let playerO = document.getElementById("player-o");
let btnGo = document.getElementById("go");
let currentPlayer = document.getElementById("current-player");
let joueurCourant = "X";
let compteur = 0;
let h2Message = document.getElementById("message");
let divH2Gameover = document.getElementById("gameover-container");
let resetButton = document.getElementById("reset");
let h2Gameover = document.getElementById("gameover");
let squareGrid = document.getElementById("squareGrid");
let endGame = false;
let choicePlayer = document.getElementById("choice-player-x");

let promptPlayerX = window.prompt('Quel est votre nom Joueur 1 ?', 'Joueur 1');
let promptPlayerO = window.prompt('Quel est votre nom Joueur 2 ?', 'Joueur 2');
playerX.innerHTML = promptPlayerX;
playerO.innerHTML = promptPlayerO;

btnGo.addEventListener("click", () =>{
    gameIn.classList.remove("hidden");
    menuDemarrage.classList.add("hidden");

    if (choicePlayer.checked !== false) {
        currentPlayer.innerHTML = promptPlayerX;
        joueurCourant = "X";
    } else {
        currentPlayer.innerHTML = promptPlayerO;
        joueurCourant = "O";
    }
});

const allDivs = document.querySelectorAll(".grid > div");
allDivs.forEach(square => {
    square.addEventListener("click", event => {
        const element = event.target;
        console.log(element);

        if (endGame === false) {
            if (element.innerHTML === "") {
                element.innerHTML = joueurCourant;

                if (calculateWinner() !== null) {
                    console.log(calculateWinner());
                    h2Message.classList.add("hidden");
                    divH2Gameover.classList.remove("hidden");
                    h2Gameover.innerHTML = calculateWinner() + " a gagné !";

                    if (joueurCourant === "X") {
                        element.classList.add("player-x");
                    } else {
                        element.classList.add("player-o");
                    }
                } else {
                    if (element.innerHTML === "X" || element.innerHTML === "O") {
                        element.classList.add("full");
                        compteur++;
                        console.log(compteur);
                    }

                    if (joueurCourant === "X") {
                        joueurCourant = "O";
                        currentPlayer.innerHTML = promptPlayerO;
                        element.classList.add("player-x");
                    } else {
                        joueurCourant = "X";
                        currentPlayer.innerHTML = promptPlayerX;
                        element.classList.add("player-o");
                    }
                }
            }

            if (compteur === 9) {
                h2Message.classList.add("hidden");
                divH2Gameover.classList.remove("hidden");
                h2Gameover.innerHTML = "Personne n'a gagné !";
                endGame = true;
            }
        }
    });
});

resetButton.addEventListener("click", () => {
    allDivs.forEach(square => {
        square.innerHTML = "";
        square.classList.remove("full");
        square.classList.remove("win-square");
        square.classList.remove("player-x");
        square.classList.remove("player-o");
    });
    compteur = 0;
    h2Message.classList.remove("hidden");
    divH2Gameover.classList.add("hidden");
    endGame = false;
    squareGrid.classList.remove("endGame");
    gameIn.classList.add("hidden");
    menuDemarrage.classList.remove("hidden");
});

function calculateWinner() {
    const casesWin = [
        [0, 1, 2],
        [3, 4, 5],
        [6, 7, 8],
        [0, 3, 6],
        [1, 4, 7],
        [2, 5, 8],
        [0, 4, 8],
        [2, 4, 6]
    ];

    for (const caseWin of casesWin) {
        if (allDivs[caseWin[0]].innerHTML === allDivs[caseWin[1]].innerHTML && allDivs[caseWin[1]].innerHTML === allDivs[caseWin[2]].innerHTML && allDivs[caseWin[2]].innerHTML === allDivs[caseWin[0]].innerHTML && allDivs[caseWin[0]].innerHTML !== "") {
            endGame = true;
            squareGrid.classList.add("endGame");
            allDivs[caseWin[0]].classList.add("win-square");
            allDivs[caseWin[1]].classList.add("win-square");
            allDivs[caseWin[2]].classList.add("win-square");
            return currentPlayer.innerHTML;
        }
    };
    return null;
};

